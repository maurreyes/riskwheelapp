package com.example.riskwheel

import android.Manifest
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.PopupMenu
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.FragmentActivity
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.navigation.NavigationView

//Some class implementations may be interact different bassed in Gradle Extensions and configurations
class MapsActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener, NavigationView.OnNavigationItemSelectedListener{

    //Variables
    lateinit var toolbar: Toolbar   //Upper toolbar
    lateinit var drawerLayout: DrawerLayout //Contains Google Maps view

    lateinit var navView: NavigationView    //Google Maps Navigation variable
    private lateinit var mMap: GoogleMap //Google map view variable
    private lateinit var fusedLocationClient: FusedLocationProviderClient //Location Client
    private lateinit var lastLocation: Location

    companion object{
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1 //Variable that enable the permission request to user
        private const val REQUEST_CHECK_SETTINGS = 2
    }

    override fun onMarkerClick(p0: Marker?) = false //Follow actual location

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        drawerLayout = findViewById(R.id.drawer_layout)
        navView = findViewById(R.id.nav_view)

        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, R.string.app_name2, 0
        )

        //Google Maps implementation
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        navView.setNavigationItemSelectedListener(this)

        //Google maps implementation into DrawerLayoutMenu
        //USE ONLY supportFrgmentManager BeginTransaction
        val mapFragment = SupportMapFragment.newInstance()
        supportFragmentManager.beginTransaction().add(R.id.map, mapFragment).commit()
        mapFragment.getMapAsync(this)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        //fab.setOnClickListener { cardview.visibility = View.VISIBLE } Cardview for typemap
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    //Function to invoke native Google Map function
    override fun onMapReady(googleMap: GoogleMap) {
        //Add Map Controls (Geolocation, zoom and Google maps aperture to follow location)
                mMap = googleMap
                mMap.uiSettings.isZoomControlsEnabled = true//Zoom in maps enabled
                mMap.moveCamera(CameraUpdateFactory.zoomBy(17f))
                setUpMap()


        //Invoke Markers in Maps
        mMap.setOnMapClickListener { latlng ->
            mMap.animateCamera(CameraUpdateFactory.newLatLng(latlng));
            val location = LatLng(latlng.latitude, latlng.longitude)
            mMap.addMarker(MarkerOptions().position(location))
        }
    }

    //setUpMap function include:
    //-Permission by user to get his location
    //-Real time Location
    private fun setUpMap() {
        if (ActivityCompat.checkSelfPermission(this, //Check if the permission was granted by user in actual installation
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        { //if isn't
            ActivityCompat.requestPermissions(this, //Require permission
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST_CODE)
            return
        }
        mMap.isMyLocationEnabled = true //Layer draws a blue dot like user location, also create user's location button to reach it.

        //mMap.mapType = GoogleMap.MAP_TYPE_HYBRID //Gives different maptypes
        fusedLocationClient.lastLocation.addOnSuccessListener(this) { //Gives the most recently location available
                location ->
            if(location != null) {
                lastLocation = location
                val currentLastLong = LatLng(location.latitude, location.longitude)
       //         placeOnMarker(currentLastLong)
                //mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLastLong, 12f)) //Default 17f
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLastLong, 12.0f))
            }
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_profile -> {
                Toast.makeText(this, "Profile clicked", Toast.LENGTH_SHORT).show()
            }
            R.id.nav_messages -> {
                Toast.makeText(this, "Messages clicked", Toast.LENGTH_SHORT).show()
            }
            R.id.nav_friends -> {
                Toast.makeText(this, "Friends clicked", Toast.LENGTH_SHORT).show()
            }
            R.id.nav_update -> {
                Toast.makeText(this, "Update clicked", Toast.LENGTH_SHORT).show()
            }
            R.id.nav_logout -> {
                Toast.makeText(this, "Sign out clicked", Toast.LENGTH_SHORT).show()
            }
        }
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    fun ChangeType(view: View) {
        when(mMap.mapType) {
            GoogleMap.MAP_TYPE_NORMAL -> { mMap.mapType = GoogleMap.MAP_TYPE_SATELLITE }
            GoogleMap.MAP_TYPE_SATELLITE -> { mMap.mapType = GoogleMap.MAP_TYPE_NORMAL }
        }
    }

}